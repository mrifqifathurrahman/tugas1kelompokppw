from django import forms

class AddFriend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi!',
    }
    name_attributes = {
        'type' : 'text',
	'class' : 'name_input_form_text_area',
        'rows' : 4,
	'cols' : 100,
	'placeholder' : "Isi dengan nama teman anda...",
    }
    url_attributes = {
	'type' : 'text',
	'class' : 'url_input_form_text_area',
	'rows' : 4,
	'cols' : 100,
	'placeholder' : "Isi dengan url teman anda...",
	}

    name = forms.CharField(label='Name ',required=True,max_length=30,widget=forms.TextInput(attrs=name_attributes))
    url  = forms.URLField(label='URLs ',required=True,max_length=100,widget=forms.URLInput(attrs=url_attributes))
    

from django.shortcuts import render
from .models import AddFriend
from .forms import AddFriend_Form
from django.http import HttpResponseRedirect
# Create your views here.
response = {}
def index(request):
    response['author'] = "Intan Wulandari"
    addfriend = AddFriend.objects.all()
    response['addfriend'] = addfriend
    html = 'add_friend/add_friend.html'
    response['addfriend_form'] = AddFriend_Form
    return render(request, html, response)


def add(request):
    form = AddFriend_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
	    response['name'] = request.POST['name']
	    response['url'] = request.POST['url']
	    friend = AddFriend(name = response['name'],url = response['url'])
	    friend.save()
	    return HttpResponseRedirect('/add-friend/')
    else:
	    return HttpResponseRedirect('/add-friend/')


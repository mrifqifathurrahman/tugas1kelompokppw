from .models import AddFriend
from .forms import AddFriend_Form
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

class AddFriendUnitTest(TestCase):
    def test_add_friend_url_is_exist(self):
        response = Client().get('/add-friend/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add-friend/')
        self.assertEqual(found.func, index)
        
    def test_model_can_create_new_addfriend(self):
        #add new friend
        new_friend = AddFriend.objects.create(name='intan',url='intanrangers.herokuapp.com')

        #Retrieving friend
        counting_all_available_addfriend = AddFriend.objects.all().count()
        self.assertEqual(counting_all_available_addfriend,1)

    def test_form_AddFriend_input_has_placeholder_and_css_classes(self):
        form = AddFriend_Form()
        self.assertIn('class="name_input_form_text_area', form.as_p())
        self.assertIn('class="url_input_form_text_area', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = AddFriend_Form(data={'name': '', 'url': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['url'],
            ["This field is required."]
        )

    def test_AddFriend_post_success_and_render_the_result(self):
        name = 'hehe'
        url = 'hehe@herokuapp.com'
        response = Client().post('/add-friend/add', {'name': name, 'url': url})
        self.assertEqual(response.status_code, 302)

        response = Client().get('/add-friend/')
        html_response = response.content.decode('utf8')
        self.assertIn(name,html_response)
        self.assertIn(url,html_response)
    

    def test_AddFriend_post_error_and_render_the_result(self):
        test= 'Hidden'
        response_post = Client().post('/add-friend/add', {'status': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/add-friend/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from update_status.models import Status
from add_friend.models import AddFriend
# Create your tests here.

class Tugas1DashboardUnitTest(TestCase):

	def test_Dashboard_url_is_exist(self):
		response = Client().get('/Dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_Dashboard_using_index_func(self):
		found = resolve('/Dashboard/')
		self.assertEqual(found.func, index)

	def test_Dashboard_can_load_data_from_status_database(self):
		new_status = Status.objects.create(status='Mengerjakan Tugas PPW nich')

		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

		response = Client().get('/Dashboard/')
		html_response = response.content.decode('utf8')
		self.assertIn(new_status.status, html_response)
		self.assertIn(str(counting_all_available_status), html_response)

	def test_Dashboard_can_load_data_from_friends_database(self):
		new_friend = AddFriend.objects.create(name='Anonymous',url='Anonymous.herokuapp.com')

		#Retrieving friend
		counting_all_available_addfriend = AddFriend.objects.all().count()
		self.assertEqual(counting_all_available_addfriend,1)

		response = Client().get('/Dashboard/')
		html_response = response.content.decode('utf8')
		self.assertIn(str(counting_all_available_addfriend), html_response)

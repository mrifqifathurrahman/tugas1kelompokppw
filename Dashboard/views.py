from django.shortcuts import render
from update_status.models import Status
from add_friend.models import AddFriend

# Create your views here.

response = {}
def index(request):
	response['latest_post'] = Status.objects.last()
	response['feed'] = Status.objects.all().count()
	response['friends'] = AddFriend.objects.all().count()
	html = 'Dashboard/Dashboard.html'
	return render(request, html, response)
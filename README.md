# Tugas 1 - Kelas B - Kelompok 13

Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

Anggota Kelompok:
	- Adib Yusril Wafi
	- Intan Wulandari
	- Mohammad Ahnaf Zain Ilham
	- Muhammad Rifqi Fathurrahman

Staus Pipeline:
[![pipeline status](https://gitlab.com/mrifqifathurrahman/tugas1kelompokppw/badges/master/pipeline.svg)](https://gitlab.com/mrifqifathurrahman/tugas1kelompokppw/commits/master)
	

Status Coverage:
[![coverage report](https://gitlab.com/mrifqifathurrahman/tugas1kelompokppw/badges/master/coverage.svg)](https://gitlab.com/mrifqifathurrahman/tugas1kelompokppw/commits/master)
	

Link herokuapp:
https://tugas1-13.herokuapp.com/

Link Repo Tugas 2:
https://gitlab.com/mrifqifathurrahman/Tugas2kelompokPPW.git
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Profile

 # TODO Implement this
expertise = ['otomotif','berenang','fotografi']
name ='Hepzibah Smith'
birthday = '01 Feb'
gender = 'male'
description = 'saya kuliah di Fasilkom Universitas Indonesia'
email = 'hello@world.com'
response = {'author':'Kelompok 13'}
# Create your views here.

def index(request):
	try:
		default = Profile.objects.get(id=1)
	except:
		default = Profile.objects.create(name="Hepzibah Smith", birthday="01 Feb", gender="Male", expertise="otomotif, berenang, fotografi", description="saya kuliah di Fasilkom Universitas Indonesia", email="hello@world.com", id=1)
		default.save()
	finally:
		default = Profile.objects.get(id=1)
		splitExpertise = default.expertise.split(',')
		response['default'] = default
		response['expertise'] = splitExpertise
		return render(request, 'halaman_profile.html', response)

def edit_profile(request):

	response['name'] = name
	profile = Profile.objects.all()
	response['profile'] = profile

	return render(request, 'edit_profile.html', response)

# def post_custom_profile(request):
	
# 	if request.method == 'POST' and form.is_valid():
# 		response['name'] = request.POST['name'] #if request.POST['name'] != ""
# 		response['birthday'] = request.POST['birthday']
# 		response['gender'] = request.POST['gender']
# 		response['expertise'] = request.POST['expertise']
# 		response['description'] = request.POST['description']
# 		response['email'] = request.POST['email']
# 		newProfile = AccountProfile(name=response['name'],birthday=response['birthday'],gender=response['gender'],expertise=response['expertise'],description=response['description'],email=response['email'])
# 		newProfile.save()
# 		html = 'edit_profile.html'
# 		render(request, html, response)
# 		return HttpResponseRedirect('/halaman-profile/')
# 	else:
# 		return HttpResponseRedirect('/edit-profile/')

def submit(request):
	profileBaru = Profile.objects.first()
	profileBaru.name = request.POST['name']
	profileBaru.birthday = request.POST['birthday']
	profileBaru.gender = request.POST['gender']
	profileBaru.expertise = request.POST['expertise']
	profileBaru.description = request.POST['description']
	profileBaru.email = request.POST['email']
	profileBaru.save()
	return HttpResponseRedirect('/halaman-profile/')

# Create your views here.

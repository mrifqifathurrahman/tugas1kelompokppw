from django.db import models

class Profile(models.Model):
	name = models.CharField(max_length=27)
	birthday = models.TextField(max_length=27)
	gender = models.TextField(max_length=27)
	expertise = models.TextField(max_length=160)
	description = models.TextField(max_length=160)
	email = models.EmailField()


# Create your models here.

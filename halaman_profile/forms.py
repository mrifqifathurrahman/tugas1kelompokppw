from django import forms

class EditProfile_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    name_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan nama...'
    }
    birth_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan tanggal lahir...'
    }
    gender_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan gender...'
    }
    email_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan email...'
    }
    expertise_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan expertise...'
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Masukan deskripsi...'
    }
    

    name = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    birth = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=birth_attrs))
    gender = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=gender_attrs))
    email = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=email_attrs))
    expertise = forms.CharField(label='', required=True, max_length=100, widget=forms.TextInput(attrs=expertise_attrs))
    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))

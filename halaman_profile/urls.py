from django.conf.urls import url
from .views import index,edit_profile,submit

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^edit_profile', edit_profile, name='edit_profile'),
	url(r'^submit', submit, name='submit'),

]
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Profile

class ProfileUnitTest(TestCase):

	def test_halaman_profile_url_is_exist(self):
		response = Client().get('/halaman-profile/')
		self.assertEqual(response.status_code, 200)

	def test_halaman_profile_using_index_func(self):
		found = resolve('/halaman-profile/')
		self.assertEqual(found.func, index)

	def test_edit_profile_using_edit_profile_func(self):
		response = Client().get('/halaman-profile/edit_profile/')
		self.assertEqual(response.status_code, 200)

		html_response = response.content.decode('utf8')
		self.assertIn("Hepzibah Smith", html_response)
		self.assertIn("01 Feb", html_response)
		self.assertIn("Male", html_response)
		self.assertIn("otomotif, berenang, fotografi", html_response)
		self.assertIn("saya kuliah di Fasilkom Universitas Indonesia", html_response)
		self.assertIn("hello@world.com", html_response)


	# def test_model_bisa_simpan_data(self):
	# 	response = Client().post('/halaman-profile/submit/', {"name":'Hepzibah Smith', "birthday":'01 Feb', 
	# 	"gender":'Male', "expertise":'otomotif, berenang, fotografi', "description":'saya kuliah di fasilkom universitas indonesia', 
	# 	"email":'hello@world.com'})
	# 	self.assertEqual(response, 302)

	# 	response = Client().get('/halaman-profile/')
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn("Hepzibah Smith",html_response)

	def test_model_bisa_buat_akun_baru(self):
		account_baru = Profile.objects.create(name='Hepzibah Smith', birthday='01 Feb', 
		gender='Male', expertise='otomotif, berenang, fotografi', description='saya kuliah di fasilkom universitas indonesia', 
		email='hello@world.com')

		hitung_semua_akun =  Profile.objects.all().count()
		self.assertEqual(hitung_semua_akun,1)

	def test_apa_gitu(self):
		test = "Jihan Adibah"
		account_baru = Profile.objects.create(name='Hepzibah Smith', birthday='01 Feb', 
		gender='Male', expertise='otomotif, berenang, fotografi', description='saya kuliah di fasilkom universitas indonesia', 
		email='hello@world.com')
		response_post = Client().post('/halaman-profile/submit/', {"name":test, "birthday":'31 Jan', 
		"gender":'Female', "expertise":'hehe, berenang, main', "description":'saya kuliah di Fasilkom ', 
		"email":'haha@hehe.com'})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/halaman-profile/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)
		self.assertIn('31 Jan', html_response)
		self.assertIn("Female", html_response)
		self.assertIn("hehe", html_response)
		self.assertIn("berenang", html_response)
		self.assertIn("main", html_response)
		self.assertIn("saya kuliah di Fasilkom", html_response)
		self.assertIn("haha@hehe.com", html_response)	


# Create your tests here.

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Update_Status_Form
from .models import Status

# Create your views here.
response = {}
def index(request):
	response['author'] = "Mohammad Ahnaf Zain Ilham"
	status = Status.objects.all()
	response['status'] = status
	html = 'update_status/update_status.html'
	response['update_status_form'] = Update_Status_Form
	return render(request, html, response)

def add_status(request):
	form = Update_Status_Form(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		status = Status(status=response['status'])
		status.save()
		return HttpResponseRedirect('/update-status/')
	else:
		return HttpResponseRedirect('/update-status/')
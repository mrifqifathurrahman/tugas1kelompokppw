from django import forms

class Update_Status_Form(forms.Form):
	error_message = {
		'required': 'Have nothing to post?',
	}
	update_status_attrs = {
		'type': 'text',
		'cols': 165,
		'rows': 4,
		'class': 'update-status-form-textarea',
		'placeholder': "Write your day here!"
	}

	status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=update_status_attrs))